# CaptionImageWebApp

1. install **Anaconda** on your PC, open anaconda prompt


2. create new virtual environment:
>> conda create -n *captionenv* python=3.6 pandas opencv numpy flask spyder theano matpotlib


3. activate your venv:
>> conda activate captionenv

4. then install the following packages
>> pip install tensorflow==1.14 keras==2.2.5

5. update your UPLOAD_FOLDER to your video or camera folder

5. change directory to this folder

6. start the app
>> python app.py

7. open your web browser and go test
