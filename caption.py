from keras.preprocessing.sequence import pad_sequences
from keras.preprocessing import image
from keras.models import load_model, Model

from keras.preprocessing import sequence
from keras.models import Sequential
from keras.layers import LSTM, Embedding, TimeDistributed, Dense, RepeatVector,\
                         Activation, Flatten, Reshape, concatenate, Dropout, BatchNormalization
from keras.optimizers import Adam, RMSprop
from keras.layers.wrappers import Bidirectional
from keras.layers.merge import add
from keras.applications.inception_v3 import InceptionV3
from keras import Input, layers
from keras import optimizers
from keras.applications.inception_v3 import preprocess_input
from keras.preprocessing.text import Tokenizer
from keras.utils import to_categorical

import matplotlib.pyplot as plt
import pickle
import numpy as np
from numpy import array
import string
import os
import pandas as pd
from PIL import Image
import glob
from pickle import dump, load
from time import time

import warnings
warnings.filterwarnings("ignore")


def load_doc(filename):
	file = open(filename, 'r')
	text = file.read()
	file.close()
	return text

filename = "C:/CaptionImageWebApp/Flickr8k/Flickr8k_text/Flickr8k.token.txt"

doc = load_doc(filename)
print(doc[:300])


def load_descriptions(doc):
	mapping = dict()
	for line in doc.split('\n'):
		tokens = line.split()
		if len(line) < 2:
			continue
		image_id, image_desc = tokens[0], tokens[1:]
		image_id = image_id.split('.')[0]
		image_desc = ' '.join(image_desc)
		if image_id not in mapping:
			mapping[image_id] = list()
		mapping[image_id].append(image_desc)
	return mapping

descriptions = load_descriptions(doc)
print('Loaded: %d ' % len(descriptions))

descriptions['1000268201_693b08cb0e']

def clean_descriptions(descriptions):
	table = str.maketrans('', '', string.punctuation)
	for key, desc_list in descriptions.items():
		for i in range(len(desc_list)):
			desc = desc_list[i]
			desc = desc.split()
			desc = [word.lower() for word in desc]
			desc = [w.translate(table) for w in desc]
			desc = [word for word in desc if len(word)>1]
			desc = [word for word in desc if word.isalpha()]
			desc_list[i] =  ' '.join(desc)

clean_descriptions(descriptions)
descriptions['1000268201_693b08cb0e']

def save_descriptions(descriptions, filename):
	lines = list()
	for key, desc_list in descriptions.items():
		for desc in desc_list:
			lines.append(key + ' ' + desc)
	data = '\n'.join(lines)
	file = open(filename, 'w')
	file.write(data)
	file.close()

save_descriptions(descriptions, 'C:/CaptionImageWebApp/Flickr8k/descriptions.txt')

def load_set(filename):
	doc = load_doc(filename)
	dataset = list()
	for line in doc.split('\n'):
		if len(line) < 1:
			continue
		identifier = line.split('.')[0]
		dataset.append(identifier)
	return set(dataset)

filename = 'C:/CaptionImageWebApp/Flickr8k/Flickr8k_text/Flickr_8k.trainImages.txt'
train = load_set(filename)
print('Dataset: %d' % len(train))

images = 'C:/CaptionImageWebApp/Flickr8k/Flicker8k_Dataset/'
img = glob.glob(images + '*.jpg')

dirpath = os.getcwd()
path_to_train_id_file = 'C:/CaptionImageWebApp/Flickr8k/Flickr8k_text/Flickr_8k.trainImages.txt'
path_to_test_id_file = 'C:/CaptionImageWebApp/Flickr8k/Flickr8k_text/Flickr_8k.testImages.txt'
path_to_image_folder = 'C:/CaptionImageWebApp/Flickr8k/Flicker8k_Dataset'


all_train_images_in_folder = glob.glob(path_to_image_folder + '/*.jpg')

train_images = set(open(path_to_train_id_file, 'r').read().strip().split('\n'))

train_img = []
for images in all_train_images_in_folder :
    if images[len(path_to_image_folder+'/'):] in train_images:
        train_img.append(images)

test_images = set(open(path_to_test_id_file, 'r').read().strip().split('\n'))
test_img = []

for images in all_train_images_in_folder:
    if images[len(path_to_image_folder+'/'):] in test_images:
        test_img.append(images)

def load_clean_descriptions(filename, dataset):
	doc = load_doc(filename)
	descriptions = dict()
	for line in doc.split('\n'):
		tokens = line.split()
		image_id, image_desc = tokens[0], tokens[1:]
		if image_id in dataset:
			if image_id not in descriptions:
				descriptions[image_id] = list()
			desc = 'startseq ' + ' '.join(image_desc) + ' endseq'
			descriptions[image_id].append(desc)
	return descriptions

train_descriptions = load_clean_descriptions('C:/CaptionImageWebApp/Flickr8k/descriptions.txt', train)
print('Descriptions: train=%d' % len(train_descriptions))


all_train_captions = []
for key, val in train_descriptions.items():
    for cap in val:
        all_train_captions.append(cap)
len(all_train_captions)

word_count_threshold = 10
word_counts = {}
nsents = 0
for sent in all_train_captions:
    nsents += 1
    for w in sent.split(' '):
        word_counts[w] = word_counts.get(w, 0) + 1

vocab = [w for w in word_counts if word_counts[w] >= word_count_threshold]
print('preprocessed words %d -> %d' % (len(word_counts), len(vocab)))

ixtoword = {}
wordtoix = {}

ix = 1
for w in vocab:
    wordtoix[w] = ix
    ixtoword[ix] = w
    ix += 1

vocab_size = len(ixtoword) + 1
vocab_size

def to_lines(descriptions):
	all_desc = list()
	for key in descriptions.keys():
		[all_desc.append(d) for d in descriptions[key]]
	return all_desc

def max_length(descriptions):
	lines = to_lines(descriptions)
	return max(len(d.split()) for d in lines)

max_length = max_length(train_descriptions)
print('Description Length: %d' % max_length)

def preprocess(image_path):
    img = image.load_img(image_path, target_size=(299, 299))
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
    x = preprocess_input(x)
    return x
global model
model = InceptionV3(weights='imagenet')
model_new = Model(model.input, model.layers[-2].output)
model_new.make_predict_function()

def encode(image):
    image = preprocess(image)
    fea_vec = model_new.predict(image)
    fea_vec = np.reshape(fea_vec, fea_vec.shape[1])
    return fea_vec

glove_dir = 'C:/CaptionImageWebApp/Flickr8k/'
embeddings_index = {}
f = open(os.path.join(glove_dir, 'glove.6B.200d.txt'), encoding="utf-8")

for line in f:
    values = line.split()
    word = values[0]
    coefs = np.asarray(values[1:], dtype='float32')
    embeddings_index[word] = coefs
f.close()
print(glove_dir)
print('Found %s word vectors.' % len(embeddings_index))

embeddings_index['the']

embedding_dim = 200

embedding_matrix = np.zeros((vocab_size, embedding_dim))

for word, i in wordtoix.items():
    embedding_vector = embeddings_index.get(word)
    if embedding_vector is not None:
        embedding_matrix[i] = embedding_vector

embedding_matrix.shape
inputs1 = Input(shape=(2048,))
fe1 = Dropout(0.5)(inputs1)
fe2 = Dense(256, activation='relu')(fe1)
inputs2 = Input(shape=(max_length,))
se1 = Embedding(vocab_size, embedding_dim, mask_zero=True)(inputs2)
se2 = Dropout(0.5)(se1)
se3 = LSTM(256)(se2)
decoder1 = add([fe2, se3])
decoder2 = Dense(256, activation='relu')(decoder1)
outputs = Dense(vocab_size, activation='softmax')(decoder2)
model = Model(inputs=[inputs1, inputs2], outputs=outputs)
model.summary()
model.layers[2].set_weights([embedding_matrix])
model.layers[2].trainable = False

model.compile(loss='categorical_crossentropy', optimizer='adam')

def data_generator(descriptions, photos, wordtoix, max_length, num_photos_per_batch):
    X1, X2, y = list(), list(), list()
    n=0
    while 1:
        for key, desc_list in descriptions.items():
            n+=1
            photo = photos[key+'.jpg']
            for desc in desc_list:
                seq = [wordtoix[word] for word in desc.split(' ') if word in wordtoix]
                for i in range(1, len(seq)):
                    in_seq, out_seq = seq[:i], seq[i]
                    in_seq = pad_sequences([in_seq], maxlen=max_length)[0]
                    out_seq = to_categorical([out_seq], num_classes=vocab_size)[0]
                    X1.append(photo)
                    X2.append(in_seq)
                    y.append(out_seq)
            if n==num_photos_per_batch:
                yield [[array(X1), array(X2)], array(y)]
                X1, X2, y = list(), list(), list()
                n=0

model.optimizer.learning_rate = 0.0001
epochs = 10
number_pics_per_bath = 6
steps = len(train_descriptions)//number_pics_per_bath

model.load_weights('C:/CaptionImageWebApp/Flickr8k/model_weights/model_30.h5')
model.summary()

images = 'C:/CaptionImageWebApp/Flickr8k/Flicker8k_Dataset/'
with open("C:/CaptionImageWebApp/Flickr8k/Pickle/encoded_test_images.pkl", "rb") as encoded_pickle:
    encoding_test = load(encoded_pickle)

def predict_caption(photo):
    in_text = 'startseq'
    for i in range(max_length):
        sequence = [wordtoix[w] for w in in_text.split() if w in wordtoix]
        sequence = pad_sequences([sequence], maxlen=max_length)
        yhat = model.predict([photo,sequence], verbose=0)
        yhat = np.argmax(yhat)
        word = ixtoword[yhat]
        in_text += ' ' + word
        if word == 'endseq':
            break
    final = in_text.split()
    final = final[1:-1]
    final = ' '.join(final)
    return final

def caption_this_image(photo):
    photo = encode(photo).reshape(1,2048)
    caption = predict_caption(photo)
    return caption
# importing the necessary libraries 
import cv2 
import os
import pyodbc

#get the video's path and get video name i.e: D:/video/name.mp4 -> name
def get_video_name(input_video, suffix):
    end = int(input_video.rindex(suffix))
    start = int(input_video.rindex('/')) + 1
    video_name = input_video[start:end]
    return video_name

#calculate the similar rate between 2 strings ie: string1 = 'dog running in the grass', string2 = 'dog is running on the grass'
#>>>same_rate(string1, string2) >>> 0.8
def same_rate(prev, cur):
    temp = cur.split(' ')
    same = list(filter(lambda x: (prev.find(x) > -1) , temp))
    rate = len(same)/len(prev.split(' '))
    return rate

#initialize the connection to SQL server
conn = pyodbc.connect('Driver={SQL Server};'
                      'Server=CHINH;'
                      'Database=CaptionImage;'
                      'Trusted_Connection=yes;')
#define the cursor in pyodbc
cursor = conn.cursor()

#generate captions from video and optimize them, save them to server SQL
def caption_this_video(input_video, similar_rate):
    #create temporary folder called data to save the temporary caption image
    try:
        #creating a folder named data
        if not os.path.exists('data'):
            os.mkdir('data')
    except OSError:
        print('Error: Existed folder!')

    #delete the previous data in Caption Table
    #delete_all_records = '''truncate table Caption'''
    #cursor.execute(delete_all_records)
    #conn.commit()    
    
    cap = cv2.VideoCapture(input_video)
    frame_number = cap.get(cv2.CAP_PROP_FRAME_COUNT)
    fps = int(cap.get(cv2.CAP_PROP_FPS))
    seconds = float(frame_number / fps)
    duration = int(seconds*1000)
    print('frame num = ' + str(frame_number) + ' fps = ' + str(fps) + ' duration = ' + str(duration) + 'ms')
    video_name = get_video_name(input_video, '.mp4')
    print('Video name: ' + video_name + '.mp4')
    video_id = video_name+"_"+str(fps)+"fps"+str(duration)+"mslong"
    print(video_id)
    check_available_query = '''SELECT count(*) FROM Caption WHERE VideoId = ?'''
    cursor.execute(check_available_query,video_id)
    rowcount = cursor.fetchone()[0]
    if rowcount == 0:
        count = 0
        start = 0
        stop = 1
        prev_caption = ''
        #Insert query
        insert_records = '''INSERT INTO Caption(VideoId ,Start, Stop, Decription) VALUES(?,?,?,?)'''
        while(True):
            ret, frame = cap.read()
            if ret:
                cap.set(cv2.CAP_PROP_POS_MSEC, (count*500))
                temp_path = './data/' + video_name + '.jpg'
                cv2.imwrite(temp_path, frame)
                current_caption = caption_this_image(temp_path)
                r = same_rate(current_caption, prev_caption)
                if count == 0:
                    print('Start at: '  + str(count*500) + 'ms :' + current_caption)
                    prev_caption = current_caption
                elif (count > 0) and (r >= similar_rate):
                    delta = duration-(count*500)
                    if (delta < 500 and delta > 0):
                        cursor.execute(insert_records, video_id,str(start*500) + 'ms' , str(duration) + 'ms', current_caption)
                        conn.commit()
                        print('Done')
                    else:
                        print('skip-------------------------------------------------------' + str(count))
                else:
                    stop = count
                    cursor.execute(insert_records, video_id,str(start*500) + 'ms' , str((stop)*500) + 'ms', prev_caption)
                    conn.commit()
                    print(str(count*500) + 'ms :' + current_caption)
                    start = stop
                    prev_caption = current_caption
                    
                os.remove(temp_path)
                count += 1
            else:
                print('DONE 100%')
                break
        cap.release()
        cv2.destroyAllWindows()
        return video_id
    else:
        print('this video is already processed')
        return video_id

import re
from functools import reduce
#split function: split a list into list of list by a element inside the list
def split(iterable, where):
    def splitter(acc, item, where=where):
        if item == where:
            acc.append([])
        else:
            acc[-1].append(item)
        return acc
    return reduce(splitter, iterable, [[]])

#get timeline of a object (keyword)
def get_timeline(keyword, video_id):
    timeline = [0]
    cursor.execute("SELECT * FROM Caption WHERE Decription like ?  and VideoId = ?", '%{}%'.format(keyword), video_id) 
    rows = cursor.fetchall()
    for row in rows:
        start = re.sub("[^0-9]", "",row.Start)
        end = re.sub("[^0-9]", "",row.Stop)
        if (int(start) - timeline[-1]) >= 2000:
            timeline.append('break')
            timeline.append(int(start))
        else:
            if int(start) not in timeline:
                timeline.append(int(start))
        timeline.append(int(end))
    timeline = split(timeline, 'break')
    return timeline

import shutil
from moviepy.video.io.ffmpeg_tools import ffmpeg_extract_subclip
from moviepy.editor import VideoFileClip, concatenate_videoclips

def process_video(path, timeline, keyword):
    video_name = get_video_name(path, '.mp4')
    link_subclips = []
    for t in timeline:
        if len(t) > 1:
            starttime = t[0]/1000
            endtime = t[-1]/1000
            targetname="static/"+video_name+str(timeline.index(t)+1)+"_"+keyword+".mp4"
            ffmpeg_extract_subclip(path, starttime, endtime, targetname)
            link_subclips.append(targetname)
    return link_subclips
            
