#!/usr/bin/captionev python
# coding: utf-8

# In[1]:


# Thêm thư viện
import numpy as np
from numpy import array
import pandas as pd
import matplotlib.pyplot as plt
import string
import os
from PIL import Image
import glob
from pickle import dump, load
from time import time

from keras.preprocessing import sequence
from keras.models import Sequential
from keras.layers import LSTM, Embedding, TimeDistributed, Dense, RepeatVector, Activation, Flatten, Reshape, concatenate, Dropout, BatchNormalization
from keras.optimizers import Adam, RMSprop
from keras.layers.wrappers import Bidirectional
from keras.layers.merge import add
from keras.applications.inception_v3 import InceptionV3
from keras.preprocessing import image
from keras.models import Model, load_model
from keras import Input, layers
from keras import optimizers
from keras.applications.inception_v3 import preprocess_input
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical


# In[2]:


# Đọc file các caption
def load_doc(filename):
	# open the file as read only
	file = open(filename, 'r')
	# read all text
	text = file.read()
	# close the file
	file.close()
	return text


filename = "Flickr8k/Flickr8k_text/Flickr8k.token.txt"

doc = load_doc(filename)


# In[3]:


# Lưu caption dưới dạng key value: id_image : ['caption 1', 'caption 2', 'caption 3',' caption 4', 'caption 5']
def load_descriptions(doc):
	mapping = dict()
	# process lines
	for line in doc.split('\n'):
		# split line by white space
		tokens = line.split()
		if len(line) < 2:
			continue
		# take the first token as the image id, the rest as the description
		image_id, image_desc = tokens[0], tokens[1:]
		# extract filename from image id
		image_id = image_id.split('.')[0]
		# convert description tokens back to string
		image_desc = ' '.join(image_desc)
		# create the list if needed
		if image_id not in mapping:
			mapping[image_id] = list()
		# store description
		mapping[image_id].append(image_desc)
	return mapping

descriptions = load_descriptions(doc)



# In[4]:




# In[5]:


# Preprocessing text
def clean_descriptions(descriptions):
	# prepare translation table for removing punctuation
	table = str.maketrans('', '', string.punctuation)
	for key, desc_list in descriptions.items():
		for i in range(len(desc_list)):
			desc = desc_list[i]
			# tokenize
			desc = desc.split()
			# convert to lower case
			desc = [word.lower() for word in desc]
			# remove punctuation from each token
			desc = [w.translate(table) for w in desc]
			# remove hanging 's' and 'a'
			desc = [word for word in desc if len(word)>1]
			# remove tokens with numbers in them
			desc = [word for word in desc if word.isalpha()]
			# store as string
			desc_list[i] =  ' '.join(desc)

# clean descriptions
clean_descriptions(descriptions)


# In[7]:


# Lưu description xuống file
def save_descriptions(descriptions, filename):
	lines = list()
	for key, desc_list in descriptions.items():
		for desc in desc_list:
			lines.append(key + ' ' + desc)
	data = '\n'.join(lines)
	file = open(filename, 'w')
	file.write(data)
	file.close()

save_descriptions(descriptions, 'Flickr8k/descriptions.txt')


# In[8]:


# Lấy id ảnh tương ứng với dữ liệu train, test, dev
def load_set(filename):
	doc = load_doc(filename)
	dataset = list()
	# process line by line
	for line in doc.split('\n'):
		# skip empty lines
		if len(line) < 1:
			continue
		# get the image identifier
		identifier = line.split('.')[0]
		dataset.append(identifier)
	return set(dataset)

# load training dataset (6K)
filename = 'Flickr8k/Flickr8k_text/Flickr_8k.trainImages.txt'
train = load_set(filename)


# In[9]:


# Folder chứa dữ ảnh
images = 'Flickr8k/Flicker8k_Dataset/'
# Lấy lấy các ảnh jpg trong thư mục
img = glob.glob(images + '*.jpg')


# In[10]:


#Đọc ảnh
dirpath = os.getcwd()
path_to_train_id_file = 'Flickr8k/Flickr8k_text/Flickr_8k.trainImages.txt'
path_to_test_id_file = 'Flickr8k/Flickr8k_text/Flickr_8k.testImages.txt'
path_to_image_folder = 'Flickr8k/Flicker8k_Dataset'


# In[11]:


#xử lý ảnh dùng để train
# Lấy lấy các ảnh jpg trong thư mục chứa toàn bộ ảnh Flickr

all_train_images_in_folder = glob.glob(path_to_image_folder + '/*.jpg')

# Đọc toàn bộ nội dung file danh sách các file ảnh dùng để train
train_images = set(open(path_to_train_id_file, 'r').read().strip().split('\n'))

# Danh sách ấy sẽ lưu full path vào biến train_img
train_img = []
for images in all_train_images_in_folder : # Duyệt qua tất cả các file trong folder
    if images[len(path_to_image_folder+'/'):] in train_images: # Nếu tên file của nó thuộc training set
        train_img.append(images) # Thì thêm vào danh sách ảnh sẽ dùng để train


# In[20]:


# File chứa các id ảnh để train
train_images_file = 'Flickr8k/Flickr8k_text/Flickr_8k.trainImages.txt'
# Read the train image names in a set
train_images = set(open(train_images_file, 'r').read().strip().split('\n')) #id

# Create a list of all the training images with their full path names
train_img = []

for i in img:  # img is list of full path names of all images
    if i[len(images):] in train_images:  # Check if the image belongs to training set
        train_img.append(i)  # Add it to the list of train images


# In[12]:



# In[14]:


# File chứa các id ảnh để test
#test_images_file = '/content/drive/My Drive/CaptionImage/Flickr8k/Flickr8k_text/Flickr_8k.testImages.txt'
# Read the validation image names in a set# Read the test image names in a set
#test_images = set(open(test_images_file, 'r').read().strip().split('\n')) #id

# Create a list of all the training images with their full path names
#test_img = []

#for i in img:  # img is list of full path names of all images
    #if i[len(images):] in test_images:  # Check if the image belongs to training set
        #test_img.append(i)  # Add it to the list of train images


# In[13]:


#xử lý ảnh dùng để test
test_images = set(open(path_to_test_id_file, 'r').read().strip().split('\n'))
test_img = []

for images in all_train_images_in_folder:
    if images[len(path_to_image_folder+'/'):] in test_images:
        test_img.append(images)
        


# In[14]:


# In[15]:


# Thêm 'startseq', 'endseq' cho chuỗi
def load_clean_descriptions(filename, dataset):
	# load document
	doc = load_doc(filename)
	descriptions = dict()
	for line in doc.split('\n'):
		# split line by white space
		tokens = line.split()
		# split id from description
		image_id, image_desc = tokens[0], tokens[1:]
		# skip images not in the set
		if image_id in dataset:
			# create list
			if image_id not in descriptions:
				descriptions[image_id] = list()
			# wrap description in tokens
			desc = 'startseq ' + ' '.join(image_desc) + ' endseq'
			# store
			descriptions[image_id].append(desc)
	return descriptions

# descriptions
train_descriptions = load_clean_descriptions('Flickr8k/descriptions.txt', train)



# In[16]:


#Tạo list các training caption
all_train_captions = []
for key, val in train_descriptions.items():
    for cap in val:
        all_train_captions.append(cap)
len(all_train_captions)


# In[17]:


# Chỉ lấy các từ xuất hiện trên 10 lần
word_count_threshold = 10
word_counts = {}
nsents = 0
for sent in all_train_captions:
    nsents += 1
    for w in sent.split(' '):
        word_counts[w] = word_counts.get(w, 0) + 1

vocab = [w for w in word_counts if word_counts[w] >= word_count_threshold]
print('preprocessed words %d -> %d' % (len(word_counts), len(vocab)))


# In[18]:


ixtoword = {}
wordtoix = {}

ix = 1
for w in vocab:
    wordtoix[w] = ix
    ixtoword[ix] = w
    ix += 1


# In[19]:


vocab_size = len(ixtoword) + 1 # Thêm 1 cho từ dùng để padding
vocab_size


# In[20]:


# convert a dictionary of clean descriptions to a list of descriptions
def to_lines(descriptions):
	all_desc = list()
	for key in descriptions.keys():
		[all_desc.append(d) for d in descriptions[key]]
	return all_desc

# calculate the length of the description with the most words
def max_length(descriptions):
	lines = to_lines(descriptions)
	return max(len(d.split()) for d in lines)

# determine the maximum sequence length
max_length = max_length(train_descriptions)
print('Description Length: %d' % max_length)


# In[21]:


# Load ảnh, resize về khích thước mà Inception v3 yêu cầu.
def preprocess(image_path):
    # Convert all the images to size 299x299 as expected by the inception v3 model
    img = image.load_img(image_path, target_size=(299, 299))
    # Convert PIL image to numpy array of 3-dimensions
    x = image.img_to_array(img)
    # Add one more dimension
    x = np.expand_dims(x, axis=0)
    # preprocess the images using preprocess_input() from inception module
    x = preprocess_input(x)
    return x


# In[22]:


# Load the inception v3 model
model = InceptionV3(weights='imagenet')


# In[24]:


# Tạo model mới, bỏ layer cuối từ inception v3
model_new = Model(model.input, model.layers[-2].output)
model_new._make_predict_function()


# In[25]:


# Image embedding thành vector (2048, )
def encode(image):
    image = preprocess(image) # preprocess the image
    fea_vec = model_new.predict(image) # Get the encoding vector for the image
    fea_vec = np.reshape(fea_vec, fea_vec.shape[1]) # reshape from (1, 2048) to (2048, )
    return fea_vec


# In[26]:


# Load Glove model
glove_dir = 'Flickr8k/'
embeddings_index = {} # empty dictionary
f = open(os.path.join(glove_dir, 'glove.6B.200d.txt'), encoding="utf-8")

for line in f:
    values = line.split()
    word = values[0]
    coefs = np.asarray(values[1:], dtype='float32')
    embeddings_index[word] = coefs
f.close()
print(glove_dir)
print('Found %s word vectors.' % len(embeddings_index))


# In[27]:


embeddings_index['the']


# In[28]:


embedding_dim = 200

# Get 200-dim dense vector for each of the 10000 words in out vocabulary
embedding_matrix = np.zeros((vocab_size, embedding_dim))

for word, i in wordtoix.items():
    #if i < max_words:
    embedding_vector = embeddings_index.get(word)
    if embedding_vector is not None:
        # Words not found in the embedding index will be all zeros
        embedding_matrix[i] = embedding_vector


# In[29]:


embedding_matrix.shape


# In[30]:


# Tạo model
inputs1 = Input(shape=(2048,))
fe1 = Dropout(0.5)(inputs1)
fe2 = Dense(256, activation='relu')(fe1)
inputs2 = Input(shape=(max_length,))
se1 = Embedding(vocab_size, embedding_dim, mask_zero=True)(inputs2)
se2 = Dropout(0.5)(se1)
se3 = LSTM(256)(se2)
decoder1 = add([fe2, se3])
decoder2 = Dense(256, activation='relu')(decoder1)
outputs = Dense(vocab_size, activation='softmax')(decoder2)
model = Model(inputs=[inputs1, inputs2], outputs=outputs)


# In[31]:


model.summary()


# In[32]:


# Layer 2 dùng GLOVE Model nên set weight thẳng và không cần train
model.layers[2].set_weights([embedding_matrix])
model.layers[2].trainable = False


# In[33]:


model.compile(loss='categorical_crossentropy', optimizer='adam')


# In[34]:


# data generator cho việc train theo từng batch model.fit_generator()
def data_generator(descriptions, photos, wordtoix, max_length, num_photos_per_batch):
    X1, X2, y = list(), list(), list()
    n=0
    # loop for ever over images
    while 1:
        for key, desc_list in descriptions.items():
            n+=1
            # retrieve the photo feature
            photo = photos[key+'.jpg']
            for desc in desc_list:
                # encode the sequence
                seq = [wordtoix[word] for word in desc.split(' ') if word in wordtoix]
                # split one sequence into multiple X, y pairs
                for i in range(1, len(seq)):
                    # split into input and output pair
                    in_seq, out_seq = seq[:i], seq[i]
                    # pad input sequence
                    in_seq = pad_sequences([in_seq], maxlen=max_length)[0]
                    # encode output sequence
                    out_seq = to_categorical([out_seq], num_classes=vocab_size)[0]
                    # store
                    X1.append(photo)
                    X2.append(in_seq)
                    y.append(out_seq)
            # yield the batch data
            if n==num_photos_per_batch:
                yield [[array(X1), array(X2)], array(y)]
                X1, X2, y = list(), list(), list()
                n=0


# In[35]:


model.optimizer.learning_rate = 0.0001
epochs = 10
number_pics_per_bath = 6
steps = len(train_descriptions)//number_pics_per_bath


# In[36]:


model.load_weights('Flickr8k/model_weights/model_30.h5')
model._make_predict_function()


# In[37]:


images = 'Flickr8k/Flicker8k_Dataset/'
with open("Flickr8k/Pickle/encoded_test_images.pkl", "rb") as encoded_pickle:
    encoding_test = load(encoded_pickle)


# In[38]:


# Với môi ảnh mới khi test, ta sẽ bắt đầu chuỗi với 'startseq' rồi sau đó cho vào model để dự đoán từ tiếp theo. Ta thêm từ
# vừa được dự đoán vào chuỗi và tiếp tục cho đến khi gặp 'endseq' là kết thúc hoặc cho đến khi chuỗi dài 34 từ.
def predict_caption(photo):
    in_text = 'startseq'
    for i in range(max_length):
        sequence = [wordtoix[w] for w in in_text.split() if w in wordtoix]
        sequence = pad_sequences([sequence], maxlen=max_length)
        yhat = model.predict([photo,sequence], verbose=0)
        yhat = np.argmax(yhat)
        word = ixtoword[yhat]
        in_text += ' ' + word
        if word == 'endseq':
            break
    final = in_text.split()
    final = final[1:-1]
    final = ' '.join(final)
    return final


# In[39]:


def caption_this_image (photo):
    photo = encode(photo).reshape(1,2048)
    caption = predict_caption(photo)
    return caption


# importing the necessary libraries 
import cv2 
import os

#get the video's path and get video name i.e: D:/video/name.mp4 -> name
def get_video_name(input_video, suffix):
    end = int(input_video.rindex(suffix))
    start = int(input_video.rindex('/')) + 1
    video_name = input_video[start:end]
    return video_name

import csv
#generate captions from video and optimize them, save them to server SQL
def caption_this_video(input_video, similar_rate):
    result = []
    #create temporary folder called data to save the temporary caption image
    try:
        #creating a folder named data
        if not os.path.exists('data'):
            os.mkdir('data')
    except OSError:
        print('Error: Existed folder!')

    cap = cv2.VideoCapture(input_video)
    frame_number = cap.get(cv2.CAP_PROP_FRAME_COUNT)
    fps = int(cap.get(cv2.CAP_PROP_FPS))
    seconds = float(frame_number / fps)
    duration = int(seconds*1000)
    print('frame num = ' + str(frame_number) + ' fps = ' + str(fps) + ' duration = ' + str(duration) + 'ms')
    video_name = get_video_name(input_video, '.mp4')
    print('Video name: ' + video_name + '.mp4')
    video_id = video_name+"_"+str(fps)+"fps"+str(duration)+"mslong"
    print(video_id)
    file = video_id + ".csv"
    w = csv.writer(open(file, "w"))
    while(True):
        ret, frame = cap.read()
        if ret:
            temp_path = './data/' + video_name + '.jpg'
            cv2.imwrite(temp_path, frame)
            current_caption = caption_this_image(temp_path)
            print(current_caption)
            w.writerow([current_caption])
            result.append(current_caption)
            os.remove(temp_path)
        else:
            print('DONE 100%')
            break
    cap.release()
    cv2.destroyAllWindows()
    return result
