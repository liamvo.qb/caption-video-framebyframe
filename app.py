from flask import Flask, render_template, url_for, request, redirect, flash
from TestModel30 import *
from werkzeug.utils import secure_filename
import warnings
warnings.filterwarnings("ignore")



app = Flask(__name__)
UPLOAD_FOLDER = 'data'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
@app.route('/')
def hello():
    return render_template('index.html')


@app.route('/image', methods = ['POST'])
def upload_image():
	if request.method == 'POST':
		img = request.files['image']
		print(img)
		print(img.filename)
		img.save("static/"+img.filename)
		caption = caption_this_image("static/"+img.filename)
		result_dic = {
			'image' : "static/" + img.filename,
			'description' : caption
		}
	return render_template('index.html', image_results = result_dic)

@app.route('/video', methods = ['POST'])
def upload_video():
	if request.method == 'POST':
		file = request.files['video']
		filename = secure_filename(file.filename)
		path = os.path.join(app.config['UPLOAD_FOLDER'], filename).replace("\\", "/")
		file.save(path)
		result = caption_this_video(path, 0.6)
		
	return render_template('index.html', video_results = result)




if __name__ == '__main__':
	app.run(debug = True)
